#include <gtk/gtk.h>

int main(int argc,char ** argv) {
	
	gtk_init(&argc,&argv);
	
	GtkWidget *window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	
	gtk_widget_set_size_request(window,300,200);
	
	GtkWidget *label = gtk_label_new("Hello World!");
	
	gtk_container_add(GTK_CONTAINER(window),label);
	
	g_signal_connect(G_OBJECT(window),"destroy",G_CALLBACK(gtk_main_quit),NULL);
	
	gtk_widget_show_all(window);
	
	gtk_main();
	
	return 0;
	
}
